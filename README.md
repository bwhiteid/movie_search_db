# Symfony Movie Search

This is a PHP Symfony application that performs a search using the Search API

## Getting Started

### Prerequisites

PHP 7.2.5 or higher

**PHP extensions:** 

- Ctype
- iconv
- JSON
- PCRE
- Session
- SimpleXML
- Tokenizer;

#### Install Composer

[Download](https://getcomposer.org/download/)

#### Download Symfony Binary (For Dev Production)

[Download](https://getcomposer.org/download/)

### Installing

Pull the Repo from Bitbucket

```
git clone https://bitbucket.org/bwhiteid/movie_search_db.git
```

Change into Directory, 

```
cd movie_search_db
composer install
```

Run Dev Server

```
symfony server:start
```


## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* PHP
* Symfony
* HTML5 up (HTML and CSS Templates)

## Authors

* **Brandon White** - [Website](https://brandonwhite.com.au)