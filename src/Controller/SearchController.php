<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class SearchController extends AbstractController
{
    /**
     * @Route("/search/{page}/{query}",name="search_db",requirements={"page"="\d+"})
     **/
    public function search($page = 1,$query = null)
    {
        $request = Request::createFromGlobals();
        if($query == null)
            $query = $request->request->get('query');
        
        if($query) {
            $api_data = $this->getSearch($query,$page);
            $sort_direction = $request->request->get('sort_direction');
            $sort_type = $request->request->get('sort_type');
            if($sort_direction && $sort_type)
                $api_data['results'] = $this->sortSearch($sort_direction,$sort_type,$api_data['results']);
            
            return $this->render('pages/search_results.html.twig',['data'=>$api_data,'query'=>$query,'sort'=>array($sort_type,$sort_direction)]);
        }
        else
           return $this->redirectToRoute('root_page');
    }
    
    
    
    public function sortSearch($sort_direction,$sort_type,$array) {
        if(count($array) > 0) {
            $key = (array_key_exists($sort_type,$array[0])) ? $sort_type : "title";
            $type = ($sort_direction == "asc") ? SORT_ASC : SORT_DESC;
            //Checks Values if vaules are valid
            $keyarray = array_column($array, $key);
            array_multisort($keyarray, $type, $array);
            return $array;
        }
        else
            return $array;
    }

    public function getSearch($query,$page)
    {
        //Performs a Get Request to the API to search the data.
        $api = 'a0af7d03e0dd03c0b9898bc8da621765';
        $url = 'http://api.themoviedb.org/3/search/movie?include_adult=false&api_key='.$api.'&query='.$query;
        if ($query) {
            $client = HttpClient::create();
            if(is_numeric($page))
                $url .= "&page=".$page;
            $response = $client->request('GET', $url);
            $statusCode = $response->getStatusCode();
            if($statusCode == 200)
            {
                /*
                Returned Value
                array: [
                  "page" => number
                  "total_results" => number
                  "total_pages" => number
                  "results" => array
                ]*/
                
                $contentType = $response->getHeaders()['content-type'][0];
                $content = $response->toArray();
                return $content;
            }
            else
            {
                return $this->render('error.html.twig',['data'=>$response,'msg'=>'API Error']);
            }
        }
    }
}
