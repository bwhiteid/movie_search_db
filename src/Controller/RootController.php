<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RootController extends AbstractController
{

	/**
	* @Route("/",name="root_page")
	*/

    public function root()
    {
        return $this->render('pages/index.html.twig');
    }
    
    /**
	* @Route("/svg/{per}",name="percent_svg")
	*/
    
    public function display_svg($per) {
        $response = $this->render('pages/percent.svg.twig',['per'=>$per]);
        $response->headers->set('Content-Type', 'image/svg+xml');
        return $response;
    }
}
