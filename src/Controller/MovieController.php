<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Routing\Annotation\Route;

class MovieController extends AbstractController
{
    /**
     * @Route("/movie/{id}", name="movie_info")
     **/
    public function movie($id)
    {
        if($id)
        {
            $api_data = $this->getMovieInfo($id);
            return $this->render('movies/movie_about.html.twig',['movie'=>$api_data]);
        }
        else
            return $this->redirectToRoute('root_page');
    }
    public function getMovieInfo($id)
    {
        //Performs a Get Request to the API to search the data.
        $api = 'a0af7d03e0dd03c0b9898bc8da621765';
        $url = 'http://api.themoviedb.org/3/movie/'.$id.'?api_key='.$api;
        if ($id) {
            $client = HttpClient::create();
            $response = $client->request('GET', $url);
            $statusCode = $response->getStatusCode();
            if($statusCode == 200)
            {
                $contentType = $response->getHeaders()['content-type'][0];
                $content = $response->toArray();
                return $content;
            }
            else
            {
                return $this->render('error.html.twig',['data'=>$response,'msg'=>'API Error']);
            }
        }
    }
}
